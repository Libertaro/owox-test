<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 24.12.2018
 * Time: 10:55
 */

class TasksTest extends \PHPUnit\Framework\TestCase
{
    public function testTask1()
    {
        require_once ('task1.php');
        $result = checkBrackets('[5] * 3 - ( 4 - 7 * [3-6])');
        $this->assertTrue($result);
    }

    public function testTask2()
    {
        require_once ('task2.php');
        $result = luckyTickets(6);
        $this->assertEquals(55252, $result);
    }
}