<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 23.12.2018
 * Time: 16:32
 */

/**
 * @param $s string
 * @return bool
 */
function checkBrackets($s)
{
    $stack = new SplStack();

    $array_openings = ['(', '['];
    $array_closes = [')', ']'];

    for ($i = 0; $i < strlen($s); $i++) {
        if ($stack->isEmpty()) {
            if (array_search($s[$i], $array_closes) !== false) {
                return false;
            }
        }

        if (array_search($s[$i], $array_openings) !== false) {
            $stack->push($s[$i]);
        }

        if (array_search($s[$i], $array_closes) !== false && array_search($stack->top(), $array_openings) === array_search($s[$i], $array_closes)) {
            $stack->pop();
        }

    }

    return $stack->isEmpty() ? true : false;
}