<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 23.12.2018
 * Time: 17:14
 */

/**
 * @param $k
 * @return int
 */
function luckyTickets($k)
{
    $half = ($k / 2);
    $variantsCountArray = [];
    $luckyTicketsCount = 0;

    for ($i = 1; $i <= $half; $i++) {
        $sum_max = $i * 9 + 1;
        if ($i === 1) {
            for ($j = 0; $j < $sum_max; $j++)
                $variantsCountArray[$i][$j] = 1;
        } else {
            $sum = 0;
            for ($j = 0; $j <= $sum_max / 2; $j++) {
                $sum += $variantsCountArray[$i - 1][$j];
                if ($j >= 10)
                    $sum -= $variantsCountArray[$i - 1][$j - 10];
                $variantsCountArray[$i][$j] = $sum;
            }
            for (; $j < $sum_max; $j++) {
                $variantsCountArray[$i][$j] = $variantsCountArray[$i][$sum_max - 1 - $j];
            }
        }
    }

    for ($i = 0; $i <= $half * 9; $i++) {
        $luckyTicketsCount += $variantsCountArray[$half][$i] * $variantsCountArray[$half][$i];
    }

    return $luckyTicketsCount;
}