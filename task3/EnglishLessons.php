<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 24.12.2018
 * Time: 12:02
 */

include_once 'School.php';
include_once 'HourlyRate.php';
include_once 'FixedRate.php';
include_once 'Grammar.php';
include_once 'Speaking.php';

class EnglishLessons
{
    public function calculate()
    {
        $school = new School();

        $twoHoursLessonPrice = new HourlyRate(2);
        $firstLesson = new Grammar(2, $twoHoursLessonPrice);
        $secondLesson = new Speaking(2, $twoHoursLessonPrice);

        $fixedPriceLesson = new FixedRate();
        $thirdLesson = new Speaking(3, $fixedPriceLesson);

        $school->addLesson($firstLesson);
        $school->addLesson($secondLesson);
        $school->addLesson($thirdLesson);

        return $school->getTotalPrice();
    }
}