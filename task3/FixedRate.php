<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 24.12.2018
 * Time: 11:35
 */

include_once 'Rate.php';

class FixedRate extends Rate
{
    protected $price = 200;

    public function calculatePrice()
    {
        return $this->price;
    }
}