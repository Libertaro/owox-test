<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 24.12.2018
 * Time: 11:32
 */

include_once 'Lesson.php';

class Grammar extends Lesson
{
    public function __construct($duration, $rate)
    {
        $this->duration = $duration;
        $this->rate = $rate;
    }
}