<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 24.12.2018
 * Time: 11:38
 */

include_once 'Rate.php';

class HourlyRate extends Rate
{
    protected $price = 100;
    private $duration;
    public function __construct($duration)
    {
        $this->duration = $duration;
    }

    public function calculatePrice()
    {
        return $this->price * $this->duration;
    }
}