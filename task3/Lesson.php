<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 24.12.2018
 * Time: 11:32
 */

abstract class Lesson
{
    public $duration;
    public $rate;

    abstract public function __construct($duration, $rate);

    public function getLessonPrice()
    {
        return $this->rate->calculatePrice();
    }
}