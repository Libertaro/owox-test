<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 24.12.2018
 * Time: 11:32
 */

abstract class Rate
{
    abstract public function calculatePrice();
}