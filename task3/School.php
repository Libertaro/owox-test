<?php
/**
 * Created by PhpStorm.
 * User: akv0725
 * Date: 24.12.2018
 * Time: 11:32
 */

class School
{
    protected $lessons = [];

    public function addLesson($lesson)
    {
        $this->lessons[] = $lesson;
    }

    public function getTotalPrice()
    {
        $total = 0;
        foreach ($this->lessons as $lesson) {
            $total += $lesson->getLessonPrice();
        }

        return $total;
    }
}